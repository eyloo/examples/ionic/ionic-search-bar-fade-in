# Search Bar Fade In

Fade in / fade out search bar using Ionic Animations

## Preview

<img src="https://gitlab.com/eyloo/examples/ionic/search-bar-fade-in/-/raw/main/_preview/preview.gif" width="400px"/>
